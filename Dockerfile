FROM centos

RUN yum -y update
RUN yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
RUN yum -y install yum-plugin-*
RUN yum -y update
RUN yum -y install sudo wget curl aria2 socat p7zip p7zip-gui p7zip-plugins zip unzip
RUN sudo yum -y localinstall --nogpgcheck https://download1.rpmfusion.org/free/el/rpmfusion-free-release-7.noarch.rpm https://download1.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-7.noarch.rpm
RUN sudo yum -y update
RUN sudo yum groupinstall -y "GNOME Desktop"
RUN sudo yum -y update
RUN sudo yum install -y tigervnc-server yum-plugin-copr
RUN sudo yum -y copr enable ivoarch/Tilix
RUN sudo yum -y install http://opensource.wandisco.com/centos/7/git/x86_64/wandisco-git-release-7-2.noarch.rpm
RUN sudo yum -y update
RUN sudo yum -y install tilix*
RUN sudo yum -y install git git-core git-all
RUN curl --silent --location https://rpm.nodesource.com/setup_8.x | sudo bash -
RUN curl -sL https://dl.yarnpkg.com/rpm/yarn.repo | sudo tee /etc/yum.repos.d/yarn.repo
RUN sudo yum -y update
RUN sudo yum -y install nodejs yarn
RUN sudo yum -y groupinstall 'Development Tools'
RUN cd /usr/local/bin && wget https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip && unzip *.zip && rm -rf *.zip
RUN yum -y clean all && rm -rf /var/cache/yum/*

#ENTRYPOINT ["/usr/lib/systemd/systemd"]